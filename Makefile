include Makefile.d/defaults.mk

all: ## Build the program (DEFAULT)
all: test build
.PHONY: all

build: # Compile the website into the public/ directory
build: base_url ?= ""
build: public
.PHONY: build

test:
	zola check
.PHONY: test


install: ## Install the content into the ${prefix}/ directory
install: prefix ?= $(out)
install: public
install:
	test "$(prefix)"
	mkdir --parents "$(prefix)"
	cp --recursive $</* $(prefix)/
.PHONY: install


public: base_url ?= ""
public:
	zola build --base-url "$(base_url)"
	touch $@

### DEVELOPMENT

develop: ## Watch and serve a development version of the website
develop:
	zola serve
.PHONY: develop

serve: ## Serve the built webiste
serve: public
serve:
	miniserve --index=index.html --interfaces=127.0.0.1 "$<"
.PHONY: serve

clean: ## Remove all build artifacts
clean:
	git clean -dfX \
		--exclude='!.envrc.private' \
		--exclude='!private/**/*'
.PHONY: clean

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs
.PHONY: result

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)


### OCI BUILD IMAGE RELATED

build-image: ## Create an OCI image with build environment (to be used in CI)
build-image: flake.nix
build-image: flake.lock
build-image:
	nix build \
		--print-build-logs \
		.#docker-image
	cp --dereference result $@

load-build-image: ## Load the OCI image using Podman
load-build-image: build-image
	gzip --decompress $^ --to-stdout | podman image load
.PHONY: load-build-image

build-inside-container: ## Build the program in an OCI container (e.g. to test CI)
build-inside-container: load-build-image
	# We need to know the project name to know which image to load. Can we do better?
	test $${project_name}
	podman run \
		--rm \
		--interactive \
		--tty \
		--volume ${PWD}:/src \
		--workdir /src \
		--entrypoint make \
		localhost/$(project_name)-build-image
.PHONY: build-inside-container
